cluster	Gene_ID
cluster1_green_Ath	AT1G01140
cluster1_green_Ath	AT1G01950
cluster1_green_Ath	AT1G03210
cluster1_green_Ath	AT1G03430
cluster1_green_Ath	AT1G03940
cluster1_green_Ath	AT1G05650
cluster1_green_Ath	AT1G06570
cluster1_green_Ath	AT1G07020
cluster1_green_Ath	AT1G07130
cluster1_green_Ath	AT1G07150
cluster1_green_Ath	AT1G07220
cluster1_green_Ath	AT1G08350
cluster1_green_Ath	AT1G08670
cluster1_green_Ath	AT1G09815
cluster1_green_Ath	AT1G10170
cluster1_green_Ath	AT1G10740
cluster1_green_Ath	AT1G11000
cluster1_green_Ath	AT1G11125
cluster1_green_Ath	AT1G11540
cluster1_green_Ath	AT1G11960
cluster1_green_Ath	AT1G12420
cluster1_green_Ath	AT1G12790
cluster1_green_Ath	AT1G12950
cluster1_green_Ath	AT1G12970
cluster1_green_Ath	AT1G13180
cluster1_green_Ath	AT1G14750
cluster1_green_Ath	AT1G15125
cluster1_green_Ath	AT1G15400
cluster1_green_Ath	AT1G15900
cluster1_green_Ath	AT1G16220
cluster1_green_Ath	AT1G16370
cluster1_green_Ath	AT1G16480
cluster1_green_Ath	AT1G16540
cluster1_green_Ath	AT1G17160
cluster1_green_Ath	AT1G17230
cluster1_green_Ath	AT1G17500
cluster1_green_Ath	AT1G17550
cluster1_green_Ath	AT1G18160
cluster1_green_Ath	AT1G18590
cluster1_green_Ath	AT1G18900
cluster1_green_Ath	AT1G19240
cluster1_green_Ath	AT1G19300
cluster1_green_Ath	AT1G19330
cluster1_green_Ath	AT1G19670
cluster1_green_Ath	AT1G19840
cluster1_green_Ath	AT1G19970
cluster1_green_Ath	AT1G20700
cluster1_green_Ath	AT1G21070
cluster1_green_Ath	AT1G21280
cluster1_green_Ath	AT1G23220
cluster1_green_Ath	AT1G23490
cluster1_green_Ath	AT1G23560
cluster1_green_Ath	AT1G24100
cluster1_green_Ath	AT1G25550
cluster1_green_Ath	AT1G26200
cluster1_green_Ath	AT1G27740
cluster1_green_Ath	AT1G27910
cluster1_green_Ath	AT1G29050
cluster1_green_Ath	AT1G29120
cluster1_green_Ath	AT1G29300
cluster1_green_Ath	AT1G30110
cluster1_green_Ath	AT1G30540
cluster1_green_Ath	AT1G31220
cluster1_green_Ath	AT1G31310
cluster1_green_Ath	AT1G31340
cluster1_green_Ath	AT1G31460
cluster1_green_Ath	AT1G32330
cluster1_green_Ath	AT1G32640
cluster1_green_Ath	AT1G34220
cluster1_green_Ath	AT1G35140
cluster1_green_Ath	AT1G37130
cluster1_green_Ath	AT1G42470
cluster1_green_Ath	AT1G43780
cluster1_green_Ath	AT1G44160
cluster1_green_Ath	AT1G47480
cluster1_green_Ath	AT1G47530
cluster1_green_Ath	AT1G48320
cluster1_green_Ath	AT1G50440
cluster1_green_Ath	AT1G51340
cluster1_green_Ath	AT1G52050
cluster1_green_Ath	AT1G52070
cluster1_green_Ath	AT1G52565
cluster1_green_Ath	AT1G54170
cluster1_green_Ath	AT1G54400
cluster1_green_Ath	AT1G54960
cluster1_green_Ath	AT1G55080
cluster1_green_Ath	AT1G55110
cluster1_green_Ath	AT1G55120
cluster1_green_Ath	AT1G55460
cluster1_green_Ath	AT1G55530
cluster1_green_Ath	AT1G56260
cluster1_green_Ath	AT1G58070
cluster1_green_Ath	AT1G58100
cluster1_green_Ath	AT1G58170
cluster1_green_Ath	AT1G58200
cluster1_green_Ath	AT1G63090
cluster1_green_Ath	AT1G63820
cluster1_green_Ath	AT1G64100
cluster1_green_Ath	AT1G64385
cluster1_green_Ath	AT1G64760
cluster1_green_Ath	AT1G65840
cluster1_green_Ath	AT1G66570
cluster1_green_Ath	AT1G66680
cluster1_green_Ath	AT1G66760
cluster1_green_Ath	AT1G66830
cluster1_green_Ath	AT1G67340
cluster1_green_Ath	AT1G67530
cluster1_green_Ath	AT1G68230
cluster1_green_Ath	AT1G68310
cluster1_green_Ath	AT1G69430
cluster1_green_Ath	AT1G69550
cluster1_green_Ath	AT1G70700
cluster1_green_Ath	AT1G70780
cluster1_green_Ath	AT1G70990
cluster1_green_Ath	AT1G72300
cluster1_green_Ath	AT1G72750
cluster1_green_Ath	AT1G74340
cluster1_green_Ath	AT1G74450
cluster1_green_Ath	AT1G74950
cluster1_green_Ath	AT1G76065
cluster1_green_Ath	AT1G76440
cluster1_green_Ath	AT1G76560
cluster1_green_Ath	AT1G76850
cluster1_green_Ath	AT1G77360
cluster1_green_Ath	AT1G77370
cluster1_green_Ath	AT1G77890
cluster1_green_Ath	AT1G78000
cluster1_green_Ath	AT1G78420
cluster1_green_Ath	AT1G78955
cluster1_green_Ath	AT1G79380
cluster1_green_Ath	AT1G80190
cluster1_green_Ath	AT1G80290
cluster1_green_Ath	AT2G01150
cluster1_green_Ath	AT2G02060
cluster1_green_Ath	AT2G03020
cluster1_green_Ath	AT2G03505
cluster1_green_Ath	AT2G11810
cluster1_green_Ath	AT2G14255
cluster1_green_Ath	AT2G14750
cluster1_green_Ath	AT2G16595
cluster1_green_Ath	AT2G16700
cluster1_green_Ath	AT2G16760
cluster1_green_Ath	AT2G17430
cluster1_green_Ath	AT2G17480
cluster1_green_Ath	AT2G17530
cluster1_green_Ath	AT2G17570
cluster1_green_Ath	AT2G19050
cluster1_green_Ath	AT2G19590
cluster1_green_Ath	AT2G19800
cluster1_green_Ath	AT2G20080
cluster1_green_Ath	AT2G20110
cluster1_green_Ath	AT2G20610
cluster1_green_Ath	AT2G21480
cluster1_green_Ath	AT2G25940
cluster1_green_Ath	AT2G26240
cluster1_green_Ath	AT2G27150
cluster1_green_Ath	AT2G27260
cluster1_green_Ath	AT2G28510
cluster1_green_Ath	AT2G30400
cluster1_green_Ath	AT2G30740
cluster1_green_Ath	AT2G30780
cluster1_green_Ath	AT2G30990
cluster1_green_Ath	AT2G31570
cluster1_green_Ath	AT2G33585
cluster1_green_Ath	AT2G33700
cluster1_green_Ath	AT2G34140
cluster1_green_Ath	AT2G34360
cluster1_green_Ath	AT2G35290
cluster1_green_Ath	AT2G36090
cluster1_green_Ath	AT2G36580
cluster1_green_Ath	AT2G37590
cluster1_green_Ath	AT2G38000
cluster1_green_Ath	AT2G38960
cluster1_green_Ath	AT2G40020
cluster1_green_Ath	AT2G41350
cluster1_green_Ath	AT2G41450
cluster1_green_Ath	AT2G42760
cluster1_green_Ath	AT2G42950
cluster1_green_Ath	AT2G44130
cluster1_green_Ath	AT2G44360
cluster1_green_Ath	AT2G44730
cluster1_green_Ath	AT2G46080
cluster1_green_Ath	AT2G46170
cluster1_green_Ath	AT2G46210
cluster1_green_Ath	AT2G46735
cluster1_green_Ath	AT2G47680
cluster1_green_Ath	AT2G47750
cluster1_green_Ath	AT3G01080
cluster1_green_Ath	AT3G01610
cluster1_green_Ath	AT3G01640
cluster1_green_Ath	AT3G02150
cluster1_green_Ath	AT3G03520
cluster1_green_Ath	AT3G03530
cluster1_green_Ath	AT3G03860
cluster1_green_Ath	AT3G04570
cluster1_green_Ath	AT3G05380
cluster1_green_Ath	AT3G05450
cluster1_green_Ath	AT3G05830
cluster1_green_Ath	AT3G06890
cluster1_green_Ath	AT3G07320
cluster1_green_Ath	AT3G10400
cluster1_green_Ath	AT3G10950
cluster1_green_Ath	AT3G11260
cluster1_green_Ath	AT3G12030
cluster1_green_Ath	AT3G12200
cluster1_green_Ath	AT3G13110
cluster1_green_Ath	AT3G14200
cluster1_green_Ath	AT3G14395
cluster1_green_Ath	AT3G15530
cluster1_green_Ath	AT3G16300
cluster1_green_Ath	AT3G16350
cluster1_green_Ath	AT3G16940
cluster1_green_Ath	AT3G16990
cluster1_green_Ath	AT3G17390
cluster1_green_Ath	AT3G17770
cluster1_green_Ath	AT3G18010
cluster1_green_Ath	AT3G18440
cluster1_green_Ath	AT3G19190
cluster1_green_Ath	AT3G19210
cluster1_green_Ath	AT3G19553
cluster1_green_Ath	AT3G20040
cluster1_green_Ath	AT3G21220
cluster1_green_Ath	AT3G21295
cluster1_green_Ath	AT3G22930
cluster1_green_Ath	AT3G23440
cluster1_green_Ath	AT3G24050
cluster1_green_Ath	AT3G24130
cluster1_green_Ath	AT3G24503
cluster1_green_Ath	AT3G25210
cluster1_green_Ath	AT3G26420
cluster1_green_Ath	AT3G26840
cluster1_green_Ath	AT3G29010
cluster1_green_Ath	AT3G30380
cluster1_green_Ath	AT3G43270
cluster1_green_Ath	AT3G44730
cluster1_green_Ath	AT3G44735
cluster1_green_Ath	AT3G45650
cluster1_green_Ath	AT3G46720
cluster1_green_Ath	AT3G47830
cluster1_green_Ath	AT3G48570
cluster1_green_Ath	AT3G50850
cluster1_green_Ath	AT3G52360
cluster1_green_Ath	AT3G52540
cluster1_green_Ath	AT3G52561
cluster1_green_Ath	AT3G53000
cluster1_green_Ath	AT3G53990
cluster1_green_Ath	AT3G54110
cluster1_green_Ath	AT3G56030
cluster1_green_Ath	AT3G56060
cluster1_green_Ath	AT3G57090
cluster1_green_Ath	AT3G57880
cluster1_green_Ath	AT3G59010
cluster1_green_Ath	AT3G59220
cluster1_green_Ath	AT3G59850
cluster1_green_Ath	AT3G59900
cluster1_green_Ath	AT3G60490
cluster1_green_Ath	AT3G61160
cluster1_green_Ath	AT3G62990
cluster1_green_Ath	AT3G63240
cluster1_green_Ath	AT4G01710
cluster1_green_Ath	AT4G02360
cluster1_green_Ath	AT4G03430
cluster1_green_Ath	AT4G04800
cluster1_green_Ath	AT4G05120
cluster1_green_Ath	AT4G05170
cluster1_green_Ath	AT4G08160
cluster1_green_Ath	AT4G10170
cluster1_green_Ath	AT4G10380
cluster1_green_Ath	AT4G11680
cluster1_green_Ath	AT4G12000
cluster1_green_Ath	AT4G12010
cluster1_green_Ath	AT4G12020
cluster1_green_Ath	AT4G12790
cluster1_green_Ath	AT4G13040
cluster1_green_Ath	AT4G13830
cluster1_green_Ath	AT4G15140
cluster1_green_Ath	AT4G15360
cluster1_green_Ath	AT4G16350
cluster1_green_Ath	AT4G16760
cluster1_green_Ath	AT4G17695
cluster1_green_Ath	AT4G18630
cluster1_green_Ath	AT4G19140
cluster1_green_Ath	AT4G19980
cluster1_green_Ath	AT4G19990
cluster1_green_Ath	AT4G20300
cluster1_green_Ath	AT4G20880
cluster1_green_Ath	AT4G20970
cluster1_green_Ath	AT4G23885
cluster1_green_Ath	AT4G24015
cluster1_green_Ath	AT4G24430
cluster1_green_Ath	AT4G24730
cluster1_green_Ath	AT4G24970
cluster1_green_Ath	AT4G25410
cluster1_green_Ath	AT4G26140
cluster1_green_Ath	AT4G26170
cluster1_green_Ath	AT4G26810
cluster1_green_Ath	AT4G26990
cluster1_green_Ath	AT4G27460
cluster1_green_Ath	AT4G28290
cluster1_green_Ath	AT4G28800
cluster1_green_Ath	AT4G29220
cluster1_green_Ath	AT4G30240
cluster1_green_Ath	AT4G30270
cluster1_green_Ath	AT4G31470
cluster1_green_Ath	AT4G32800
cluster1_green_Ath	AT4G33160
cluster1_green_Ath	AT4G33180
cluster1_green_Ath	AT4G34310
cluster1_green_Ath	AT4G34400
cluster1_green_Ath	AT4G36010
cluster1_green_Ath	AT4G36110
cluster1_green_Ath	AT4G36220
cluster1_green_Ath	AT4G36640
cluster1_green_Ath	AT4G37520
cluster1_green_Ath	AT4G37560
cluster1_green_Ath	AT4G38530
cluster1_green_Ath	AT4G39320
cluster1_green_Ath	AT4G39820
cluster1_green_Ath	AT4G39910
cluster1_green_Ath	AT5G01040
cluster1_green_Ath	AT5G01720
cluster1_green_Ath	AT5G01730
cluster1_green_Ath	AT5G03110
cluster1_green_Ath	AT5G04390
cluster1_green_Ath	AT5G05090
cluster1_green_Ath	AT5G05100
cluster1_green_Ath	AT5G05570
cluster1_green_Ath	AT5G07460
cluster1_green_Ath	AT5G07730
cluster1_green_Ath	AT5G07820
cluster1_green_Ath	AT5G08139
cluster1_green_Ath	AT5G08335
cluster1_green_Ath	AT5G09970
cluster1_green_Ath	AT5G10830
cluster1_green_Ath	AT5G12390
cluster1_green_Ath	AT5G12940
cluster1_green_Ath	AT5G13100
cluster1_green_Ath	AT5G13340
cluster1_green_Ath	AT5G13500
cluster1_green_Ath	AT5G13790
cluster1_green_Ath	AT5G15940
cluster1_green_Ath	AT5G17370
cluster1_green_Ath	AT5G20190
cluster1_green_Ath	AT5G20990
cluster1_green_Ath	AT5G21120
cluster1_green_Ath	AT5G21950
cluster1_green_Ath	AT5G24520
cluster1_green_Ath	AT5G25790
cluster1_green_Ath	AT5G27380
cluster1_green_Ath	AT5G27830
cluster1_green_Ath	AT5G34930
cluster1_green_Ath	AT5G36290
cluster1_green_Ath	AT5G36880
cluster1_green_Ath	AT5G37770
cluster1_green_Ath	AT5G38610
cluster1_green_Ath	AT5G39360
cluster1_green_Ath	AT5G39590
cluster1_green_Ath	AT5G39660
cluster1_green_Ath	AT5G39680
cluster1_green_Ath	AT5G40470
cluster1_green_Ath	AT5G41070
cluster1_green_Ath	AT5G41800
cluster1_green_Ath	AT5G41900
cluster1_green_Ath	AT5G42350
cluster1_green_Ath	AT5G42570
cluster1_green_Ath	AT5G43180
cluster1_green_Ath	AT5G43890
cluster1_green_Ath	AT5G44370
cluster1_green_Ath	AT5G44700
cluster1_green_Ath	AT5G44860
cluster1_green_Ath	AT5G45100
cluster1_green_Ath	AT5G45480
cluster1_green_Ath	AT5G45840
cluster1_green_Ath	AT5G46050
cluster1_green_Ath	AT5G46590
cluster1_green_Ath	AT5G47050
cluster1_green_Ath	AT5G47250
cluster1_green_Ath	AT5G48370
cluster1_green_Ath	AT5G48385
cluster1_green_Ath	AT5G48560
cluster1_green_Ath	AT5G48650
cluster1_green_Ath	AT5G48670
cluster1_green_Ath	AT5G49900
cluster1_green_Ath	AT5G50210
cluster1_green_Ath	AT5G50915
cluster1_green_Ath	AT5G51050
cluster1_green_Ath	AT5G51200
cluster1_green_Ath	AT5G51480
cluster1_green_Ath	AT5G51780
cluster1_green_Ath	AT5G52210
cluster1_green_Ath	AT5G52410
cluster1_green_Ath	AT5G52450
cluster1_green_Ath	AT5G53060
cluster1_green_Ath	AT5G54140
cluster1_green_Ath	AT5G54590
cluster1_green_Ath	AT5G56260
cluster1_green_Ath	AT5G56840
cluster1_green_Ath	AT5G57050
cluster1_green_Ath	AT5G57090
cluster1_green_Ath	AT5G57210
cluster1_green_Ath	AT5G57390
cluster1_green_Ath	AT5G58110
cluster1_green_Ath	AT5G58160
cluster1_green_Ath	AT5G61440
cluster1_green_Ath	AT5G62220
cluster1_green_Ath	AT5G63850
cluster1_green_Ath	AT5G63880
cluster1_green_Ath	AT5G64110
cluster1_green_Ath	AT5G64700
cluster1_green_Ath	AT5G65140
cluster1_green_Ath	AT5G66300
cluster1_green_Ath	AT5G66740
cluster1_green_Ath	AT5G66940
cluster1_green_Ath	AT5G67540
cluster1_green_Ath	AT5G67620