get_pi_syn=function(pi_syn,syn_length.y){
  
  pi_syn_tot=sum(pi_syn)
  length_syn_tot=sum(syn_length.y)
  
  return(pi_syn_tot/length_syn_tot)
  
}