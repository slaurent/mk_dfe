#function to do the MK-test accounting for different lengths for polymorphism and divergence

do_MK_test_rel_per_gene=function(d_0, d_0_length, d_sel, d_sel_length, P_0, P_0_length, P_sel, P_sel_length){
  
  #initialize contingency matrix
  dp_mat=matrix(data = NA,nrow = 2,  ncol = 2)
  
  #calculating alpha total
  #get relative neutral divergence
  d_0_tot=d_0
  #get relative functional divergence
  d_sel_tot=d_sel
  #get relative number of neutral polymorphims
  P_0_tot=P_0#/(d_0_length/P_0_length)
  #get relative number of selected polymorphims
  P_sel_tot=P_sel#/(d_sel_length/P_sel_length)
  
  #calculate alpha
  alpha_total=1-(d_0_tot/d_sel_tot)*(P_sel_tot/P_0_tot)
  
  #fill matrix
  dp_mat[1,1]=P_sel_tot
  dp_mat[1,2]=d_sel_tot
  dp_mat[2,1]=P_0_tot
  dp_mat[2,2]=d_0_tot
  
  #print dp matrix
  print(dp_mat)
  print(alpha_total)
  
  #do fisher´s exact test (two-sided)
  res=fisher.test(dp_mat)
  
  return(res$p.value)
}

