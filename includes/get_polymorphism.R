get_polymorphism=function(local_region){
  
  formatted_url_line=paste("/overlap/region/arabidopsis_thaliana/",local_region,"?feature=variation", sep="")
  
  r <- GET(paste(server, formatted_url_line, sep = ""), content_type("application/json"))
  
  stop_for_status(r)
  
  region=data.frame(t(sapply(content(r),c)))
  
  
  
  #return(names(table(unlist(region$source))))
  return(region)
  
  
  
  
}