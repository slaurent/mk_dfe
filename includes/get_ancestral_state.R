get_ancestral_state=function(one_variant_coordinate){
  
  #define url of REST server
  server <- "http://rest.ensembl.org"
  formatted_url_line=paste("/alignment/region/arabidopsis_thaliana/",gsub(" ","",format_gene_coordinate(one_variant_coordinate)),"?method=LASTZ_NET&compact=1;species_set=arabidopsis_thaliana;species_set=arabidopsis_lyrata;compara=plants", sep="")
  
  print(formatted_url_line)
  
  #retrieving data from server
  r <- GET(paste(server, formatted_url_line, sep = ""), content_type("application/json"))
  
  #formatting region info to be passed to error handling
  #error_message=toString(local_region)
  
  alignments=NULL
  
  if(r$status_code==200)
  {
    
    #get the alignments for this region 
    my_content=content(r)
  
    #extracting chr , position in ingroup, and alleleic states in ingroup and outgroup
    chromosome=my_content[[1]]$alignments[[1]]$seq_region
    pos=my_content[[1]]$alignments[[1]]$start
    ingroup_allele=my_content[[1]]$alignments[[1]]$seq
    outgroup_allele=my_content[[1]]$alignments[[2]]$seq
    
    return(data.frame(chromosome=chromosome, start=pos, ingroup=ingroup_allele, outgroup=outgroup_allele))
      
    #error handling
    
  } else if(r$status_code!=200) {
    
    log_file_name=paste(analysisID,"_get_ancestral_state_log_file.txt",sep="")
    cat(paste("Problem in get_ancestral_state line 33","html status code:", r$status_code,";\t", formatted_url_line, "\n"),file = log_file_name, append = T)
  }
}