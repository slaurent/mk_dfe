
get_divergent_sites=function(list_of_sub_regions)
{
  #extract sequences
  #thaliana
  seq1=unlist(strsplit(list_of_sub_regions$alignments[[1]]$seq, split=""))
  #lyrata
  seq2=unlist(strsplit(list_of_sub_regions$alignments[[2]]$seq, split=""))

  #create matrix for package "ape"
  m=matrix(c(seq1,seq2), nrow = 2, byrow = T)

  #delete all gaps
  m_no_gaps=del.colgapsonly(m, threshold = 0.0000001)

  #delete all sites with a gap in thaliana (to get positions back into TAIR10 reference system)
  #identify which columns have a gap "-" in thaliana
  if(length(which(m[1,]=="-"))>0)
  {
    m_no_gaps_in_thaliana=m[,-which(m[1,]=="-")]
  } else if(length(which(m[1,]=="-"))==0) {
    m_no_gaps_in_thaliana=m
  } else(stop("line 27 in get_divergent_sites: problem with filtering gapped sites in A. thaliana"))
  
  #extract reference transcript and aligned sequence
  seq1=m_no_gaps_in_thaliana[1,]
  seq2=m_no_gaps_in_thaliana[2,]
  
  #sanity checking: length of thaliana sequences minus the gaps should be equal tot he length of the sequence in TAIR10
  stopifnot(length(seq1)==list_of_sub_regions$alignments[[1]]$end - list_of_sub_regions$alignments[[1]]$start + 1)
  
  #identify divergent sites
  list_of_divergent_sites=which(seq1!=seq2)
  
  #identify thaliana (ref) alleles art divergent sites
  list_of_divergent_alleles_ref=as.array(seq1)[which(seq1!=seq2)]
  
  #identify lyrata (alt) alleles art divergent sites
  list_of_divergent_alleles_alt=as.array(seq2)[which(seq1!=seq2)]

  #get start position of fragment (reference system)
  start_ref=list_of_sub_regions$alignments[[1]]$start 
  
  #get chromosome of current fragment
  chr=list_of_sub_regions$alignments[[1]]$seq_region

  #convert coordinates 
  list_of_divergent_sites_ref=list_of_divergent_sites+start_ref-1
  
  #creating vector with replicated chromosome ID
  chr_vector=rep(chr, length(list_of_divergent_sites_ref))
  
  #creating vector of replicated (useless) dots (to conform to forma requirements for json body in VEP POST)
  snpID_vector=rep(".",length(list_of_divergent_sites_ref))
  
  #creating vector with gene IDs 
  geneID=rep(list_of_sub_regions$id[1],length(list_of_divergent_sites_ref))
  
  #creating a vector to contain the ungapped length (to later get the total length of the dataset)
  ungapped_length=rep(dim(m_no_gaps)[2],length(list_of_divergent_sites_ref))
  
  #pack everything together
  div_sites=cbind.data.frame(chr_vector, list_of_divergent_sites_ref, snpID_vector, list_of_divergent_alleles_ref, list_of_divergent_alleles_alt, geneID, ungapped_length)

  #exclude sites that are different because of a deletion in A. lyrata
  div_sites=subset(div_sites,div_sites$list_of_divergent_alleles_alt!="-")
  
  #calculate gene-specific upstream divergence
  #get the number of divergent sites
  div_abs=dim(div_sites)[1]
  div_rel=div_abs/dim(m_no_gaps)[2]
  
  gene_specific_divergence=data.frame(id=list_of_sub_regions$id[1], div_abs=div_abs, ungapped_length=dim(m_no_gaps)[2], div_rel=div_rel)
  
  #return(div_sites)
  return(gene_specific_divergence)

  
}
