#this function returns the coordinates of the region upstream of the specified gene. 
#it takes as input a list of gene identifiers and the size of the region to be extracted


get_upstream_coordinates=function(geneID_vector, window_size, dump_gene_info_to_file=0, analysisID=analysisID){
  
  #define server url
  server <- "http://rest.ensembl.org"
  ext <- "/lookup/id?expand=1"
  
  #declare and define body for POSTing
  my_body=toJSON(list(ids=geneID_vector))
  
  print(my_body)
  print(paste(server,ext,sep = ""))
  
  #posting to server
  r<-POST(paste(server,ext,sep = ""), content_type("application/json"), accept("application/json"), body = my_body)
  
  #checking for error status
  stop_for_status(r)
  
  #get content
  response=fromJSON(toJSON(content(r)))
  
  #write all available gene-specific information to a file (optional) (transcripts information is treated separately)
  if(dump_gene_info_to_file==1)
  {
    #easiest way i found to get rid of transcripts in all listed genes
    #variables_to_keep=names(response[[1]])[!names(response[[1]])%in%c("Transcript")]#add "display name to this list if it causes any problem (this variable is not present in each entry of the DB)
    variables_to_keep=c(names(response[[1]])[!names(response[[1]])%in%c("Transcript","display_name")],"display_name")#add "display name to this list if it causes any problem (this variable is not present in each entry of the DB)
    #extract relevant information
    gene_info_extended_df=do.call(rbind,lapply(response,"[",variables_to_keep))
    #depending on whether the first entry (i.e. response[1]) has an "dispaly name" entry or not, a header for this column may be missing in the final table. here i make sure it always present
    colnames(gene_info_extended_df)[ncol(gene_info_extended_df)]="display_name"
    #append to output file (first test and erase since this uses append)
    output_file_name=paste(analysisID,"_gene_specific_information.csv", sep="")
    if(file.exists(output_file_name)) file.remove(output_file_name)
    write.table(gene_info_extended_df, file=output_file_name, append = T,quote = F, sep="\t", row.names = F)
    save(gene_info_extended_df,file = paste(backup_directory,"/","gene_info", sep=""))
  }
  
  
  #extract relevant information
  gene_coordinates_df=ldply(lapply(response,"[",c("seq_region_name","start","end","strand","id")), data.frame)
  
  #calculate coordinates of upstream regions (taking into account the orientation of the gene)
  gene_coordinates_df$start_reg = ifelse(gene_coordinates_df$strand==1, gene_coordinates_df$start - window_size, gene_coordinates_df$end)
  gene_coordinates_df$end_reg   = ifelse(gene_coordinates_df$strand==1, gene_coordinates_df$start, gene_coordinates_df$end+window_size)
  
  #only return coordinates for the regulatory regions 
  return(gene_coordinates_df[,c("seq_region_name","start_reg","end_reg","id","strand")])

}