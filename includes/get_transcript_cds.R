get_transcript_cds=function(transcripts){
  
  #server with path to endpoint 
  server="http://rest.ensembl.org/sequence/id?type=cds;object_type=transcript"
  
  #formatting body
  my_body=toJSON(list(ids=unlist(transcripts)))
  
  #posting to server
  r<-POST(server, content_type("application/json"), accept("application/json"), body = my_body)
  
  #print(my_body)
  #print(r$status)
  
  if(r$status==200){
    
    response=fromJSON(toJSON(content(r)))
    
    #add length of transcript to object
    response$seq_length=nchar(unlist(response$seq))
    #print(paste("debugging", response$seq_length))
    
    return(response)
    
  } else {
    
    log_file_name=paste(analysisID,"_get_transcript_cds_log_file.txt",sep="")
    cat(paste("Problem in get_transcript_cds line 21", "status_code:", r$status_code, "\n"),file = log_file_name, append = F)
  }
  
}