#this function will only work if the length of the geneID_vector is < 1000. (because of POST constrinats)
# i should make this more robust by splitting this vector is chuncks of 1000 if more than 1000 genes are provided. 
# i have already done this for another POST function. 

get_largest_transcript_per_gene=function(geneID_vector)
{
  #define server url
  server <- "http://rest.ensembl.org"
  ext <- "/lookup/id?expand=1;species=arabidopsis_thaliana"
  
  #declare and define body for POSTing
  my_body=toJSON(list(ids=geneID_vector))
  
  #posting to server
  r<-POST(paste(server,ext,sep = ""), content_type("application/json"), accept("application/json"), body = my_body)
  
  #checking for error status
  stop_for_status(r)
  
  #get content
  response=fromJSON(toJSON(content(r)))
  
  
  
  #for each gene use transcript with the largest number of exons
  #find_largest_transcripts=function(exon_list){
  #  largest_exon=which.max(lapply(exon_list, function(x) length(x[,1])));
  #}
  
  #for each gene find transcript with the largest length
  find_largest_transcripts=function(transcript_df){
    largest_transcript_id=which.max(unlist(transcript_df$end) - unlist(transcript_df$start) + 1);
  }
  
  #for each gene find number of transcripts
  find_number_of_transcripts=function(transcript_df){
    num_of_transcripts=dim(transcript_df)[1];
  }
  
  #get longest transcript per gene (report Parent and transcript id into a dataframe). 
  largest.transcripts=do.call(rbind,lapply(response, function(x) x$Transcript[find_largest_transcripts(x$Transcript),c("Parent","id")]))
  names(largest.transcripts)=c("Parent","ids")
  
  return(largest.transcripts)

}