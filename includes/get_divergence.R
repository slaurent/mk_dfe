get_divergence=function(local_region)

{
  
  server <- "http://rest.ensemblgenomes.org"
  formatted_url_line=paste("/alignment/region/arabidopsis_thaliana/",local_region,"?method=LASTZ_NET;species_set=arabidopsis_thaliana;species_set=arabidopsis_lyrata", sep="")
  
  #print(paste(formatted_url_line,"\n",sep=""))
  #ext <- "/alignment/region/arabidopsis_thaliana/1:8001-18000:1?method=LASTZ_NET;species_set=arabidopsis_thaliana;species_set=arabidopsis_lyrata"
  #r <- GET(paste(server, ext, sep = ""), content_type("application/json"))
  
  r <- GET(paste(server, formatted_url_line, sep = ""), content_type("application/json"))
  
  
  stop_for_status(r)
  
  # use this if you get a simple nested list back, otherwise inspect its structure
  region=data.frame(t(sapply(content(r),c)))
  
  #get the number of subregions for this region
  num_subregions=length(region$tree)
  
  #get the alignments for this region 
  alignments=region$alignments
  
  #extract first alignment from sub_region 1
  #sub_alignment=alignments[1]
  
  div_mk=colSums(t(sapply(alignments, calculate_divergence_on_region)))
  
  #div_mk=sum(sapply(alignments, calculate_divergence_on_region))
  
  return(div_mk)
  
  
}